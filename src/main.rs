#[macro_use]
extern crate lazy_static;

use std::thread;
use std::sync::{Arc, Condvar, Mutex};
use std::time::Duration;

use clap::{Arg, App};

mod client;
mod server;

fn main() {
    let matches = App::new("MJPEG Proxy")
        .version("1.0.0")
        .author("Nathan Monfils <nathan.monfils@hotmail.fr>")
        .about("Proxies an MJPEG stream")
        .arg(
            Arg::with_name("port")
                .short("p")
                .long("port")
                .value_name("PORT")
                .help("Port to listen on")
                .takes_value(true)
                .required(true)
                .index(1)
        )
        .arg(
            Arg::with_name("url")
                .short("u")
                .long("url")
                .value_name("URL")
                .help("URL to connect to")
                .takes_value(true)
                .required(true)
                .index(2)
        )
        .get_matches();

    let listen_port = matches.value_of("port")
        .unwrap()
        .parse::<u16>()
        .expect("Port is not a valid number!");

    let url = matches.value_of("url").unwrap();

    let last_jpeg = Arc::new(Mutex::new(Vec::<u8>::new()));
    let jpeg_change = Arc::new(Condvar::new());

    println!("Listening on port {}", listen_port);
    let _last_jpeg = last_jpeg.clone();
    let _jpeg_change = jpeg_change.clone();
    thread::spawn(move || {
        server::run(_last_jpeg, _jpeg_change, listen_port);
    });

    loop {
        let _last_jpeg = last_jpeg.clone();
        let _jpeg_change = jpeg_change.clone();
        println!("Running MJPEG server client...");
        client::run(_last_jpeg, _jpeg_change, url).unwrap_or_else(|e| {
            println!("Error running client: {}", e);
        });
        thread::sleep(Duration::from_millis(1000));
    }
}
