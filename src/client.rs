use regex::Regex;
use std::sync::{mpsc::{channel, Sender}, Condvar, Arc, Mutex};
use std::{str, thread};
use std::time::Duration;

use curl::easy::Easy;
use curl::easy::WriteError;

fn parse_content_type(header: String) -> Result<String, String> {
    lazy_static! {
        static ref CONTENT_TYPE_RE: Regex = Regex::new(r"(?i)Content-Type: multipart/x-mixed-replace;.*boundary=([^;]+)\r\n").unwrap();
    }

    if ! CONTENT_TYPE_RE.is_match(header.as_str()) {
        Err(format!("Not a valid multipart/x-mixed-replace header: '{}'", header))
    } else {
        let boundary = CONTENT_TYPE_RE
            .captures(header.as_str())
            .ok_or("Could not parse Content-Type header")?
            .get(1).unwrap()
            .as_str();
        Ok("--".to_string() + boundary + "\r\n")
    }
}

const HEADERS_END: &[u8] = &[0x0d, 0x0a, 0x0d, 0x0a];

// Error is (true, msg) if fatal, else (false, msg)
// We don't want to exit if we just haven't received enough data yet, but we do want a new connection if we are receiving garbage
fn parse_content_length(data: &[u8], boundary: &String) -> Result<(usize, usize), (bool, String)> {
    if ! data.starts_with(boundary.as_bytes()) {
        return Err((true, "Boundary not found at beginning of frame".to_string()));
    }

    lazy_static! {
        static ref CONTENT_LENGTH_RE: Regex = Regex::new(r"(?i)Content-Length: ([0-9]+)$").unwrap();
    }

    let mut headers = Err((false, "Could not find end to headers.".to_string()));
    for i in 0..data.len() {
        if data[i..].starts_with(HEADERS_END) {
            headers = Ok(data[.. i + HEADERS_END.len()].to_vec())
        }
    }

    let headers = String::from_utf8(headers?)
        .map_err(|e| {
            (true, format!("Headers are not valid UTF-8: {}", e).to_string())
        })?;

    for header in headers.split("\r\n") {
        if header.to_lowercase().starts_with("content-length: ") {
            let length = CONTENT_LENGTH_RE
                .captures(header)
                .ok_or((true, "Could not parse Content-Length header".to_string()))?
                .get(1).unwrap()
                .as_str()
                .parse::<usize>().map_err(|e| {
                    (true, format!("Content-Length is not a valid number ({})", e).to_string())
                })?;
            return Ok((length, headers.as_bytes().len()));
        }
    }

    Err((true, "No Content-Type header found".to_string()))
}

fn header_function(header: String, delim_s: Sender<Option<String>>) -> Result<(), String> {
    if header.starts_with("Content-Type:") {
        delim_s
            .send(Some(parse_content_type(header)?))
            .map_err(|e| {
                format!("Could not send content-type header to main thread: {}", e).to_string()
            })?;

        Ok(())
    } else if header.is_empty() {
        delim_s.send(None).unwrap_or(());
        Err("Empty header".to_string())
    } else {
        Ok(())
    }
}

pub fn run(last_jpeg: Arc<Mutex<Vec<u8>>>, jpeg_change: Arc<Condvar>, url: &str) -> Result<(), String> {
    let mut buffer: Vec<u8> = Vec::new();

    let (data_s, data_r) = channel();
    let (delim_s, delim_r) = channel();

    let mut easy = Easy::new();
    easy.url(url).map_err(|e| {
        format!("Not a valid URL: '{}': {}", url, e).to_string()
    })?;

    let s = delim_s.clone();
    easy.header_function(move |header| {
        let header = match String::from_utf8(header.to_vec()) {
            Ok(h) => h,
            Err(e) => {
                eprintln!("Could not parse header as UTF-8: {}", e);
                return false;
            }
        };

        match header_function(header, s.clone()) {
            Ok(_) => true,
            Err(e) => {
                eprintln!("Headers parsing failed: {}", e);
                false
            }
        }
    }).map_err(|e| {
        format!("Headers parsing failed: {}", e).to_string()
    })?;

    let s = data_s.clone();
    easy.write_function(move |data| {
        s.send(Ok(Vec::from(data))).map_err(|e| {
            eprintln!("Error sending data to main thread: {}.", e);
            WriteError::Pause
        })?;
        Ok(data.len())
    }).map_err(|e| {
        format!("Error receiving data from MJPEG server: {}", e).to_string()
    })?;

    println!("Performing the request...");

    let s = data_s.clone();
    let s2 = delim_s.clone();
    thread::spawn(move || {
        let e = easy.perform().map_err(|e| {
            format!("Lost connection to the remote MJPEG stream: {}", e).to_string()
        }).unwrap_or(());

        s.send(Err("Lost connection to the remote MJPEG stream.".to_string())).unwrap_or(());
        s2.send(None).unwrap_or(());
    });

    println!("Waiting for a Content-Type header...");

    let boundary = delim_r
        .recv_timeout(Duration::from_secs(5))
        .map_err(|e| {
            format!("Could not receive data from cURL headers parser: {}", e).to_string()
        })?
        .ok_or("Did not receive a Content-Type header.")?;

    println!("Boundary is: '{}'", boundary);

    let mut content_length = None;
    let mut headers_length = None;
    loop {
        let data = data_r
            .recv_timeout(Duration::from_secs(1))
            .map_err(|e| {
                format!("Could not receive data from cURL thread: {}", e).to_string()
            })??;

        buffer.extend(data);

        if content_length.is_none() {
            match parse_content_length(buffer.as_slice(), &boundary) {
                Ok((_content_length, _headers_length)) => {
                    content_length = Some(_content_length);
                    headers_length = Some(_headers_length);
                },
                Err((fatal, e)) => {
                    if fatal {
                        return Err(e);
                    } else {
                        eprintln!("{}", e);
                        continue;
                    }
                }
            }
        }

        let mut _content_length = content_length.unwrap();
        let mut _headers_length = headers_length.unwrap();

        if buffer.len() > _content_length + _headers_length + 2 {
            {
                let mut jpeg = last_jpeg.lock().unwrap();
                jpeg.clear();
                jpeg.extend(&buffer.as_slice()[_headers_length .. _headers_length + _content_length]);
            }
            jpeg_change.notify_all();
            buffer = buffer.as_slice()[_headers_length + _content_length + 2 ..].to_vec();
            content_length = None;
            headers_length = None;
        }
    }
}
