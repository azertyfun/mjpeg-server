use std::cmp::min;
use std::sync::{Condvar, Arc, Mutex};
use std::borrow::Cow;
use std::io;
use std::io::Read;
use std::thread;
use std::time::Duration;
use std::collections::VecDeque;

use rouille::{Response, ResponseBody};
use uuid::Uuid;

const BOUNDARY: &str = "MJPEGproxyboundary";

struct MJPEGReader {
    id: Uuid,
    last_jpeg: Arc<Mutex<Vec<u8>>>,
    buf: Arc<Mutex<VecDeque<u8>>>,
    jpeg_change: Arc<Condvar>,
}

impl MJPEGReader {
    fn run(&self) {
        loop {
            let guard = self.last_jpeg.lock().unwrap();
            let jpeg = self.jpeg_change.wait(guard).unwrap().to_vec();
            let mut buf = self.buf.lock().unwrap();

            if Arc::strong_count(&self.buf) == 1 {
                println!("Client was disconnected, stopping read thread {}...", self.id);
                break;
            }

            if buf.len() < jpeg.len() * 20 {
                let mut v = Vec::<u8>::new();
                v.extend(format!("--{}\r\nContent-Type: image/jpeg\r\nContent-Length: {}\r\n\r\n", BOUNDARY, jpeg.len()).to_string().as_bytes());
                v.extend(jpeg);
                v.extend("\r\n".as_bytes());
                buf.extend(v);
            } else {
                println!("Client is dropping frames!");
            }
        }
    }
}

impl Clone for MJPEGReader {
    fn clone(&self) -> MJPEGReader {
        MJPEGReader {
            id: self.id,
            last_jpeg: self.last_jpeg.clone(),
            buf: self.buf.clone(),
            jpeg_change: self.jpeg_change.clone()
        }
    }
}

impl Read for MJPEGReader {
    fn read(&mut self, buf: &mut[u8]) -> io::Result<usize> {
        let mut size = 0;

        while size == 0 {
            size = min(buf.len(), self.buf.lock().unwrap().len());
            if size == 0 {
                thread::sleep(Duration::from_millis(10));
            }
        }

        let mut src = self.buf.lock().unwrap();

        for i in 0..size {
            buf[i] = src.pop_front().unwrap();
        }

        io::Result::Ok(size)
    }
}

fn subscribe_reader(last_jpeg: Arc<Mutex<Vec<u8>>>, jpeg_change: Arc<Condvar>) -> MJPEGReader {
    let uuid = Uuid::new_v4();
    let reader = MJPEGReader {
        id: uuid,
        last_jpeg: last_jpeg,
        buf: Arc::new(Mutex::new(VecDeque::new())),
        jpeg_change: jpeg_change
    };

    let _r = reader.clone();
    thread::spawn(move || {
        println!("Starting read thread {}", uuid);
        _r.run();
    });

    reader
}

pub fn run(last_jpeg: Arc<Mutex<Vec<u8>>>, jpeg_change: Arc<Condvar>, listen_port: u16) {
    rouille::start_server(format!("0.0.0.0:{}", listen_port), move |request| {
        if request.url() == "/image.jpg" {
            Response::from_data("image/jpeg", last_jpeg.lock().unwrap().to_vec())
        } else if request.url() == "/video" {
            println!("/video request");
            let _jpeg_change = jpeg_change.clone();
            Response {
                status_code: 200,
                headers: vec![
                    (Cow::from("Server"), Cow::from("MJPEG Proxy")),
                    (Cow::from("Conection"), Cow::from("Close")),
                    (Cow::from("Max-Age"), Cow::from("0")),
                    (Cow::from("Expires"), Cow::from("0")),
                    (Cow::from("Cache-Control"), Cow::from("no-store, no-cache, must-revalidate, pre-check=0, post-check=0, max-age=0")),
                    (Cow::from("Pragma"), Cow::from("no-cache")),
                    (Cow::from("Content-Type"), Cow::from(format!("multipart/x-mixed-replace;boundary={}", BOUNDARY))),
                ],
                data: ResponseBody::from_reader(subscribe_reader(last_jpeg.clone(), _jpeg_change)),
                upgrade: None
            }
        } else {
            let mut r = Response::text("Not found");
            r.status_code = 404;
            r
        }
    });
}